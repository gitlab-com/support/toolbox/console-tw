require 'faraday'
require 'json'
require 'yaml'

yaml = YAML.load_file('./config/application.yml')
 
class HttpConnection 
    def httpOpen(token, url)
       
        @conn = Faraday.new(
            url: url,
            headers: {'Content-Type'  => 'application/json', 
                      'Private-Token' => token
            }
        )      
    end
end

class Issue < HttpConnection
    def initialize(token, projectID)
        @projectID = projectID
        @baseURL = "https://gitlab.com/api/v4/projects/" + @projectID + "/issues/"
        @http = httpOpen(token, @baseURL)      
    end

    def create(title,description)
        # POST /projects/:id/issues
        postData = { 'title' => title, 'description' => description }
        resp = @http.post('', postData.to_json)

        issueUrl = JSON.parse(resp.body)
        issueUrl['web_url']
    end

    def close(issueID)
        # PUT /projects/:id/issues/:issue_iid?state_event=close
        url = issueID + "?state_event=close" 
        resp = @http.put(url)
        issue = JSON.parse(resp.body)
        issue['state']
    end
end

class SupportTask < HttpConnection

    def initialize(cid)
    end 

    def increaseCILimit(minutes)
    end

    def setCOMLevel(level)
    def

end


class TrainingWheel < HttpConnection

    def initialize(token)
        @baseURL = 'https://gitlab.com/api/v4/'
        @http = httpOpen(token, @baseURL)
    end

    def  query 
        resp = @http.get(@requestParams)
        @response = JSON.parse(resp.body)
    end

    def searchUser(user)
        @requestParams = "namespaces/" + user
        self.query
    end

    def searchEmail(email)
        @requestParams = 'users?search=' + email
        self.query
    end

    def response
        @response
    end

    def pretyOutput
        puts JSON.pretty_unparse(@response)
    end
    
end

### Expires 03-12
#apiToken = yaml['gitlab_token']


#begin
#    t = TrainingWheel.new(apiToken)
#    t.searchEmail('ralfaro@gitlab.com')
#    puts t.response[0]['name']
#    t.pretyOutput
    
    
    #i = Issue.new(apiToken, '16911350')
    #
    #
    #    url = i.close(11)
    #    puts url
    #url = i.create("title","description")
        
#rescue StandardError => msg  
#    puts "[!] Exception catched ¯\_(ツ)_/¯,\nCheck your application.yml ensure the token has the correct permissions associated to it."
#end

