Gem::Specification.new do |s|
  s.name        = 'supportAPIWraper'
  s.version     = '0.0.2'
  s.licenses    = ['MIT']
  s.summary     = "An API wrapper to automate some of the support tasks for .COM"
  s.description = "A class to automate support tasks against gitlab.com"
  s.authors     = ["Engineering Support"]
  s.files       = ["lib/supportApiWraper.rb"]
  s.homepage    = "https://about.gitlab.com/handbook/support/"
end
